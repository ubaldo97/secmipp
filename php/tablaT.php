<?php
include('../php/db.php');
include('../php/function.php');
$query = '';
$output = array();
$query .= "SELECT * FROM estatus ";
$column = array("estatus.nombre");
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE nombre LIKE "%'.$_POST["search"]["value"].'%" ';
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$column[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY nombre DESC ';
}
if($_POST["length"] != -1)
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();
foreach($result as $row)
{
    
	$sub_array = array();
	$sub_array[] = $row["nombre"];
	$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-primary  update" ><i class="fa fa-pencil-square-o"></i> </button>';
	$sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger  delete"><i class="fa fa-trash-o"></i></button>';
	$data[] = $sub_array;
}
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
echo json_encode($output);
?>