<?php
session_start();
include('../php/conexion.php');
$sql = "SELECT * FROM antiguedad WHERE id = '1'";
$resultado = $mysqli->query($sql);
$row = $resultado->fetch_array(MYSQLI_ASSOC);
if (isset($_SESSION['usuario']))
{
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SECMIPP</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- date-range-picker -->
    <!-- Alertas -->
    <link rel="stylesheet" href="../iziToast/dist/css/iziToast.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="../admin/admin.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SECMIPP</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="../php/salir.php">
                  <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo "Salir"?></span>
                </a>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- =============================================== -->
      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['usuario']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menú Principal</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Buscar docente/administrativo</span>
              </a>
              
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Buscar cursos capturados</span>
              </a>
            </li>
            <li>
              <a href="../widgets.html">
                <i class="fa fa-table"></i> <span>Registrar Docente/Administrativo</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Situación laboral</span>
                
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Listado General del Escalafón</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Imprimir Listado Basificados </span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Imprimir Listado Homologados</span>
              </a>
            </li>
            <li>
              <a href="../calendar.html">
                <i class="fa fa-table"></i> <span>Integrantes de la Comisión</span>
              </a>
            </li>
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Catálogos</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="cicloE.php"><i class="fa fa-circle-o"></i>Ciclo escolar</a></li>
                <li><a href="cicloEV.php"><i class="fa fa-circle-o"></i>Ciclo escolar vigente</a></li>
                <li><a href="estatusT.php"><i class="fa fa-circle-o"></i>Estatus del trabajador</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Fecha de cálculo de antiguedad</a></li>
                <li><a href="homologacion.php"><i class="fa fa-circle-o"></i>Tipos de homologación</a></li>
                <li><a href="planteles.php"><i class="fa fa-circle-o"></i>Planteles/Centros</a></li>
                <li><a href="preparacion.php"><i class="fa fa-circle-o"></i>Niveles de preparación profesional</a></li>
                <li><a href="cursos.php"><i class="fa fa-circle-o"></i>Tipos de cursos</a></li>
                <li><a href="trabajador.php"><i class="fa fa-circle-o"></i>Tipo de trabajador</a></li>
                <li><a href="desempeno.php"><i class="fa fa-circle-o"></i>Valores desempeño profesional</a></li>
                <li><a href="usuario.php"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i>Roles</a></li>
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i><strong>Nuevo proceso de captura</strong></a></li>
              </ul>
            </li>
            <li><a href="creditos.php"><i class="fa fa-book"></i> <span>Créditos</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- ===============jjajajajajaja================================ -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Fecha del Cálculo de Antigüedad <br><br>
          </h1>
        </section>
        
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Selección</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Fecha:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" value="<?php echo  $row['fecha']; ?>" class="form-control pull-right" id="datepicker">
                        </div>
                        <!-- /.input group -->
                      </div>
                    </div>
                    
                  </div>
                  <div class="row">
                    <div class="col-xs-2" >
                    <button type="button" onclick="Validar(document.getElementById('datepicker').value);" class="btn btn-block btn-primary">Guardar</button></div></div>
                    <!-- /.row -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
          <strong>Comisión Mixta de Ingreso, Permanencia y Promoción</strong>
        </footer>
        
      </div>
      <!-- ./wrapper -->
      <!-- jQuery 3 -->
      <script src="../bower_components/jquery/dist/jquery.min.js"></script>
      <!-- date-range-picker -->
      <script src="../bower_components/moment/min/moment.min.js"></script>
      <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
      <!-- bootstrap datepicker -->
      <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
      <!-- bootstrap color picker -->
      <script src="../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
      <!-- bootstrap time picker -->
      <script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- SlimScroll -->
      <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
      <!-- FastClick -->
      <script src="../bower_components/fastclick/lib/fastclick.js"></script>
      <!-- AdminLTE App -->
      <script src="../dist/js/adminlte.min.js"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="../dist/js/demo.js"></script>
      
      
      <script>
      $(function () {
      
      //Date picker
      $('#datepicker').datepicker({
      autoclose: true
      })
      })
      </script>
      
      <script src="../iziToast/dist/js/iziToast.min.js" type="text/javascript"></script>
      <script>
      function Validar(datepicker)
      {
      $.ajax({
      url: "../php/editarFecha.php",
      type: "POST",
      data: "fecha="+datepicker,
      success: function(resp){
      iziToast.success({
      title: 'OK',
      message: resp,
      });
      },
      });
      
      }
      </script>
    </body>
  </html>
  <?php
  }
  else
  {
  echo '<script>location.href = "../index.php";</script>';
  }
  ?>