<?php
session_start();
include('../php/conexion.php');
$sql = "SELECT * FROM valoresdesempeno WHERE id = '1'";
$resultado = $mysqli->query($sql);
$row = $resultado->fetch_array(MYSQLI_ASSOC);
if (isset($_SESSION['usuario']))
{
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SECMIPP</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Alertas -->
    <link rel="stylesheet" href="../iziToast/dist/css/iziToast.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="../admin/admin.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SECMIPP</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="../php/salir.php">
                  <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo "Salir"?></span>
                </a>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- =============================================== -->
      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['usuario']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menú Principal</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Buscar docente/administrativo</span>
              </a>
              
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Buscar cursos capturados</span>
              </a>
            </li>
            <li>
              <a href="../widgets.html">
                <i class="fa fa-table"></i> <span>Registrar Docente/Administrativo</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Situación laboral</span>
                
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Listado General del Escalafón</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Imprimir Listado Basificados </span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Imprimir Listado Homologados</span>
              </a>
            </li>
            <li>
              <a href="../calendar.html">
                <i class="fa fa-table"></i> <span>Integrantes de la Comisión</span>
              </a>
            </li>
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Catálogos</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="cicloE.php"><i class="fa fa-circle-o"></i>Ciclo escolar</a></li>
                <li><a href="cicloEV.php"><i class="fa fa-circle-o"></i>Ciclo escolar vigente</a></li>
                <li><a href="estatusT.php"><i class="fa fa-circle-o"></i>Estatus del trabajador</a></li>
                <li><a href="fechaA.php"><i class="fa fa-circle-o"></i>Fecha de cálculo de antiguedad</a></li>
                <li><a href="homologacion.php"><i class="fa fa-circle-o"></i>Tipos de homologación</a></li>
                <li><a href="planteles.php"><i class="fa fa-circle-o"></i>Planteles/Centros</a></li>
                <li><a href="preparacion.php"><i class="fa fa-circle-o"></i>Niveles de preparación profesional</a></li>
                <li><a href="cursos.php"><i class="fa fa-circle-o"></i>Tipos de cursos</a></li>
                <li><a href="trabajador.php"><i class="fa fa-circle-o"></i>Tipo de trabajador</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Valores desempeño profesional</a></li>
                <li><a href="usuario.php"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i>Roles</a></li>
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i><strong>Nuevo proceso de captura</strong></a></li>
              </ul>
            </li>
            <li><a href="creditos.php"><i class="fa fa-book"></i> <span>Créditos</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- =============================================== -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Valores de desempeño profesional<br><br>
          </h1>
        </section>
        
        <!-- Main content -->
        <section class="content" >
          <div class="box box-info">
            
            <!-- form start -->
            <form id="id_form" class="form-horizontal" >
              <div class="box-body" >
                <div class="form-group" >
                  <label for="pmalumno" class="col-sm-6 control-label">Puntos Máximos Evaluación del Alumno</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['pmalumno']; ?>" name="pmalumno" id="pmalumno"  required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="emalumno" class="col-sm-6 control-label">Evaluación Máxima del Alumno</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['emalumno']; ?>" name="emalumno" id="emalumno" required="required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="pmacademica" class="col-sm-6 control-label">Puntos Máximos Evaluación Académica</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['pmacademica']; ?>" name="pmacademica" id="pmacademica"  required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="emacademica" class="col-sm-6 control-label">Evaluación Máxima Académica</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['emacademica']; ?>" name="emacademica" id="emacademica"  required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pmaprobacion" class="col-sm-6 control-label">Puntos Máximos % Aprobación</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['pmaprobacion']; ?>" name="pmaprobacion" id="pmaprobacion" required="required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="emaprobacion" class="col-sm-6 control-label">Evaluación Máxima % Aprobación</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['emaprobacion']; ?>"  name="emaprobacion" id="emaprobacion" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pmaprovechamiento" class="col-sm-6 control-label">Puntos Máximos Prom. Aprovechamiento</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['pmaprovechamiento']; ?>" name="pmaprovechamiento" id="pmaprovechamiento" required="required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="emaprovechamiento" class="col-sm-6 control-label">Evaluación Máxima Prom. Aprovechamiento</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['emaprovechamiento']; ?>" name="emaprovechamiento" id="emaprovechamiento" required="required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="pmasistencia" class="col-sm-6 control-label">Puntos Máximos % de Asistencia</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['pmasistencia']; ?>" name="pmasistencia" id="pmasistencia" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="emasistencia" class="col-sm-6 control-label">Evaluación Máxima % de Asistencia</label>
                  <div class="col-sm-2">
                    <input type="text"  class="form-control" value="<?php echo  $row['emasistencia']; ?>" name="emasistencia" id="emasistencia" required="required">
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button onclick="Validar(document.getElementById('pmalumno').value, document.getElementById('emalumno').value, document.getElementById('pmacademica').value,  document.getElementById('emacademica').value, document.getElementById('pmaprobacion').value,document.getElementById('emaprobacion').value,document.getElementById('pmaprovechamiento').value,document.getElementById('emaprovechamiento').value,document.getElementById('pmasistencia').value,document.getElementById('emasistencia').value);" type="button" class="btn btn-info pull-right">Guardar</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </section>
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong>Comisión Mixta de Ingreso, Permanencia y Promoción</strong>
      </footer>
      
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../iziToast/dist/js/iziToast.min.js" type="text/javascript"></script>
    <script>
    function Validar(pmalumno,emalumno,pmacademica,emacademica,
    pmaprobacion,emaprobacion,pmaprovechamiento,
    emaprovechamiento,pmasistencia,emasistencia)
    {
    $.ajax({
    url: "../php/valoresDesempeno.php",
    type: "POST",
    data: "pmalumno="+pmalumno+"&emalumno="+emalumno+"&pmacademica="+pmacademica+"&emacademica="+emacademica+"&pmaprobacion="+pmaprobacion+"&emaprobacion="+emaprobacion+"&pmaprovechamiento="+pmaprovechamiento+"&emaprovechamiento="+emaprovechamiento+"&pmasistencia="+pmasistencia+"&emasistencia="+emasistencia,
    success: function(resp){
    if(resp =="0"){
    iziToast.error({
    title: 'Error',
    message: "Ocurrió un error compruebe los campos",
    });
    }else{
              iziToast.success({
    title: 'OK',
    message: "Operación exitosa",
    });
    }
    },
    });
    
    }
    </script>
    <script>
    $(document).ready(function () {
    $('.sidebar-menu').tree()
    })
    </script>
  </body>
</html>
<?php
}
else
{
echo '<script>location.href = "../index.php";</script>';
}
?>