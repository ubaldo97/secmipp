<?php
session_start();
if (isset($_SESSION['usuario']))
{
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SECMIPP</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <!-- DataTables -->
    <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Alertas -->
    <link rel="stylesheet" href="../iziToast/dist/css/iziToast.min.css">
    
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="../admin/admin.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SECMIPP</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="../php/salir.php">
                  <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo "Salir"?></span>
                </a>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- =============================================== -->
      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['usuario']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menú Principal</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Buscar docente/administrativo</span>
              </a>
              
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Buscar cursos capturados</span>
              </a>
            </li>
            <li>
              <a href="../widgets.html">
                <i class="fa fa-table"></i> <span>Registrar Docente/Administrativo</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Situación laboral</span>
                
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Listado General del Escalafón</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Imprimir Listado Basificados </span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Imprimir Listado Homologados</span>
              </a>
            </li>
            <li>
              <a href="../calendar.html">
                <i class="fa fa-table"></i> <span>Integrantes de la Comisión</span>
              </a>
            </li>
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Catálogos</span>
                <span class="pull-right-container" >
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" >
                <li><a href="cicloE.php"><i class="fa fa-circle-o"></i>Ciclo escolar</a></li>
                <li><a href="cicloEV.php"><i class="fa fa-circle-o"></i>Ciclo escolar vigente</a></li>
                <li><a href="estatusT.php"><i class="fa fa-circle-o"></i>Estatus del trabajador</a></li>
                <li><a href="fechaA.php"><i class="fa fa-circle-o"></i>Fecha de cálculo de antiguedad</a></li>
                <li><a href="homologacion.php"><i class="fa fa-circle-o"></i>Tipos de homologación</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Planteles/Centros</a></li>
                <li><a href="preparacion.php"><i class="fa fa-circle-o"></i>Niveles de preparación profesional</a></li>
                <li><a href="cursos.php"><i class="fa fa-circle-o"></i>Tipos de cursos</a></li>
                <li><a href="trabajador.php"><i class="fa fa-circle-o"></i>Tipo de trabajador</a></li>
                <li><a href="desempeno.php"><i class="fa fa-circle-o"></i>Valores desempeño profesional</a></li>
                <li><a href="usuario.php"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i>Roles</a></li>
                <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i><strong>Nuevo proceso de captura</strong></a></li>
              </ul>
            </li>
            <li><a href="creditos.php"><i class="fa fa-book"></i> <span>Créditos</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- =============================================== -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Planteles/Centros <br><br>
          </h1>
        </section>
        
        <!-- Main content -->
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Tabla de contenido</h3>
                </div>
                
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="user_data" class="table table-bordered table-striped" style="width:100%">
                    <div align="right">
                      <button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-info btn-lg">Agregar</button>
                    </div><br>
                    <thead>
                      <tr>
                        <th>Nombre del Plantel</th>
                        <th>Clave SEP</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                      </tr>
                    </thead>
                  </table>
                  
                  <div id="userModal" class="modal fade">
                    <div class="modal-dialog">
                      <form method="post" id="user_form" enctype="multipart/form-data">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Agregar Plantel/Centro</h4>
                          </div>
                          <div class="modal-body">
                            <label>Nombre del Plantel</label>
                            <input type="text" name="nombre" id="nombre" required="required" class="form-control" />
                            <br>
                            <label>Clave SEP</label>
                            <input type="text" name="clave" id="clave" minlength="10" maxlength="10" required pattern="[0-9]{2}[A-Z]{3}[0-9]{4}[A-Z]{1}" required="required" class="form-control" />
                            <br>
                            
                            <br>
                            <div class="modal-footer">
                              <input type="hidden" name="user_id" id="user_id" />
                              <input type="hidden" name="operation" id="operation" />
                              <input type="submit"  name="action" id="action" class="btn btn-success" value="Agregar" />
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                            
                          </div>
                          
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong>Comisión Mixta de Ingreso, Permanencia y Promoción</strong>
      </footer>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- DataTables -->
    <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../iziToast/dist/js/iziToast.min.js" type="text/javascript"></script>
    <!-- >Falta script y base de datos
    para manejo de los ciclos escolares  </-->
    
    <script>
    var idioma_espanol = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
    "sFirst":    "Primero",
    "sLast":     "Último",
    "sNext":     "Siguiente",
    "sPrevious": "Anterior"
    },
    "oAria": {
    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
    }
    $(document).ready(function () {
    $('.sidebar-menu').tree()
    $('#add_button').click(function(){
        $('#user_form')[0].reset();
        $('.modal-title').text("Agregar Plantel/Centro");
        $('#action').val("Agregar");
        $('#operation').val("Agregar");
        
    
      });
    
    var dataTable = $('#user_data').DataTable({
    responsive: true,
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
          url:"../php/tablaPlantel.php",
          type:"POST"
        },
    "columnDefs":[
    {
    "targets":[2,3],
    "orderable":false,
    },
    ],
    "language":idioma_espanol
    
      });
    $(document).on('submit', '#user_form', function(event){
        event.preventDefault();
    
        var nombre = $('#nombre').val();
        var clave = $('#clave').val();
    
        
          
        if(nombre != '' )
        {
          $.ajax({
            url:"../php/plantel.php",
            method:'POST',
            data:new FormData(this),
            contentType:false,
            processData:false,
            success:function(data)
            {
              iziToast.success({
    title: 'OK',
    message: data,
    });
              $('#user_form')[0].reset();
              $('#userModal').modal('hide');
              dataTable.ajax.reload();
            }
          });
        }
        else
        {
          alert("Datos Requeridos");
        }
      });
    
    $(document).on('click', '.update', function(){
        var user_id = $(this).attr("id");
        $.ajax({
          url:"../php/editarPlantel.php",
          method:"POST",
          data:{user_id:user_id},
          dataType:"json",
          success:function(data)
          {
            $('#userModal').modal('show');
            $('#nombre').val(data.nombre);
            $('#clave').val(data.clave);
            $('.modal-title').text("Editar Plantel");
            $('#user_id').val(user_id);
            $('#action').val("Editar");
            $('#operation').val("Editar");
          }
        })
      });
    
      $(document).on('click', '.delete', function(){
        var user_id = $(this).attr("id");
    iziToast.question({
    timeout: 20000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'Advertencia',
    message: 'Estas seguro de que quieres borrar este Plantel?',
    position: 'center',
    buttons: [
    ['<button><b>SI</b></button>', function (instance, toast) {
    
      $.ajax({
            url:"../php/borrarPlantel.php",
            method:"POST",
            data:{user_id:user_id},
            success:function(data)
            {
    
    
              iziToast.success({
    title: 'OK',
    message: data,
    });
              dataTable.ajax.reload();
            }
          });
    
    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
    
    }, true],
    ['<button>NO</button>', function (instance, toast) {
    
    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
    
    }],
    ],
    onClosing: function(instance, toast, closedBy){
    console.info('Closing | closedBy: ' + closedBy);
    },
    onClosed: function(instance, toast, closedBy){
    console.info('Closed | closedBy: ' + closedBy);
    }
    });
      });
    
    })
    </script>
    
    
  </body>
</html>
<?php
}
else
{
echo '<script>location.href = "../index.php";</script>';
}
?>