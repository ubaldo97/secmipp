jQuery(document).on('submit', '#form1g', function (event) {
    event.preventDefault();
    
    jQuery.ajax({
        url: '../php/login.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize(),
        beforeSend: function () {
            $('.btn-login').val('Validando...');
        }
    })
        .done(function (respuesta) {
            console.log(respuesta); 
            if (!respuesta.error) {
                if (respuesta.tipo === 'Administrador') {
                    location.href = '../admin/admin.php';
                }
            } else {
               
                alert('NO JALA');
            }
        })
        .fail(function (resp) {
            console.log(resp.responseText);
        })
        .always(function () {
            console.log("complete");
        });
});
