function validarU () {
 
    //Variables
    var usuario = document.getElementById('usuario').value;
    var usuarioInput = document.getElementById('usuario');

    var texto1 = document.getElementById('resultado1');
   
    var boton = document.getElementById('enviar');
    
    var nombre = document.getElementById('nombre').value;
    var apellido = document.getElementById('apellido').value;
   
    //La variable se incrementa si existe algún error.
    var flag = 0;
    var cont = 0;
    var cont1 = 0;
 
    if(usuario.length < 5){
        if(cont === 0){
            texto1.style.display = "block";
            texto1.value ="El usuario debe tener como mínimo 6 caracteres";
        }else{
            texto1.style.display = "block";
            texto1.value =  "El usuario debe tener como mínimo 6 caracteres";
        }
        usuarioInput.style.borderWidth = "4px";
        usuarioInput.style.background = "#e46262";
        cont++;
    }else{
        texto1.style.display = "none";
        usuarioInput.style.borderWidth = "4px";
        usuarioInput.style.background = "white";
    }
       
}

function validarP () {
 
    //Variables
   
    var pswd = document.getElementById('contrasena').value;
    var pswdInput = document.getElementById('contrasena');
    
    var texto3 = document.getElementById('resultado3');
   
    var boton = document.getElementById('enviar');
    
    var nombre = document.getElementById('nombre').value;
    var apellido = document.getElementById('apellido').value;
   
    //La variable se incrementa si existe algún error.
    var flag = 0;
    var cont = 0;
    var cont1 = 0;
 
 
    if(pswd.length < 5){
        texto3.style.display = "block";
        texto3.value =  "La contraseña debe tener como mínimo 6 dígitos";
        pswdInput.style.borderWidth = "4px";
        pswdInput.style.background = "#e46262";
        
    }else{
        texto3.style.display = "none";
        pswdInput.style.borderWidth = "4px";
        pswdInput.style.background = "white";
    }
   
}


function validarPC () {
 
    //Variables
   
    var pswd = document.getElementById('contrasena').value;
    var pswdInput = document.getElementById('contrasena');
    
    var pass = document.getElementById('confcontrasena').value;
    var passInput = document.getElementById('confcontrasena');
    
    var texto3 = document.getElementById('resultado3');
    var texto4 = document.getElementById('resultado4');
    var boton = document.getElementById('enviar');
    
    var nombre = document.getElementById('nombre').value;
    var apellido = document.getElementById('apellido').value;
   
    //La variable se incrementa si existe algún error.
    var flag = 0;
    var cont = 0;
    var cont1 = 0;
 
 
 
    if(pass === pswd ){
        texto4.style.display = "none";
        passInput.style.background = "white";
    }else{
        
        texto4.style.display = "block";
        texto4.value = "Las contraseñas no son iguales";
        passInput.style.background = "#e46262";
    }
    
   
}


function validarC () {
 
    //Variables

    var correo = document.getElementById('correo').value;
    var correoInput = document.getElementById('correo');
    
    var mail = document.getElementById('confcorreo').value;
    var mailInput = document.getElementById('confcorreo');

    var texto2 = document.getElementById('resultado2');    
   
    var boton = document.getElementById('enviar');
    
    var nombre = document.getElementById('nombre').value;
    var apellido = document.getElementById('apellido').value;
   
    //La variable se incrementa si existe algún error.
    var flag = 0;
    var cont = 0;
    var cont1 = 0;
 
   
    
    if(correo != mail || cont1>0){
        if(cont === 0){
            texto2.style.display = "block";
            texto2.value = "Las direcciones de correo no son iguales";
            mailInput.style.background = "#e46262";
        }else{
            texto2.style.display = "block";
            texto2.value = "Las direcciones de correo no son iguales";
            mailInput.style.background = "#e46262";
        }
        cont++;
    }else{
        texto2.style.display = "none";
        mailInput.style.background = "white";
    }
    
   
}